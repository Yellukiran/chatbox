

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';


import NavigationScreen from './src/screens/navigation'

const App =  () => {
  return (
    <>
      <StatusBar barStyle="light-content" />
      <SafeAreaView style={{flex: 1}}>
        <NavigationScreen/>
      </SafeAreaView>
    </>
  );
};



export default App;
