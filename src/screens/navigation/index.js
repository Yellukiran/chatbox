import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import HomeScreen from '../Home'
import PostDetailsScreen from '../PostDetailsScreen'
import PostListing from '../PostListing'

const Stack = createStackNavigator();

function NavigationScreen() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="HomeScreen">
      <Stack.Screen 
          options={{
            title: 'HOME',
            }}
        name="HomeScreen" component={HomeScreen} />
        <Stack.Screen
          name="PostDetails" component={PostDetailsScreen} 
          options={{
            title: 'COMMENTS',
          }}/>
        <Stack.Screen 
         options={{
          title: 'POSTS',
          }}
        name="PostListing" component={PostListing} />
      </Stack.Navigator>
    </NavigationContainer>
  );  
}

export default NavigationScreen