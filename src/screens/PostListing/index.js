import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, TextInput, Image, ScrollView, RefreshControl, Button, TouchableWithoutFeedback, FlatList, TouchableOpacity, } from 'react-native';
import { baseURL } from './../../config';
const trash = require('./../../assets/images/delete.png');



const ListComponent = ({ userName, item, onPress, deletePost }) => {
    return (
        <TouchableOpacity
            onPress={() => {
                onPress();
            }}>
            <View style={styles.list}>
                <View style={{ width: '100%', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                    <Text style={{ fontSize: 20, fontWeight: '700', }}>{ }</Text>
                    <TouchableOpacity
                        onPress={deletePost}
                        activeOpacity={0.9}
                        hitSlop={{
                            top: 50,
                            bottom: 50,
                            left: 50,
                            right: 50
                        }}
                    >
                        <Image source={trash} style={{ width: 25, height: 25 }} />
                    </TouchableOpacity>

                </View>
                <Text style={styles.title}
                    ellipsizeMode={'tail'}
                    numberOfLines={1}
                >{userName}</Text>
                <Text style={styles.subTitle}
                    ellipsizeMode={'tail'}
                    numberOfLines={1}>{item.title}</Text>
            </View>
        </TouchableOpacity>
    )
}


const PostListing = ({ navigation, route }) => {
    const { userId, userName } = route.params || {};
    const [refreshing, setRefreshing] = useState(false);
    const [list, setPostListing] = useState([]);
    const [users, setusersList] = useState([]);
    const [post, setNewPost] = useState(null);

    useEffect(() => {
        fetch(`${baseURL}/posts?userId=${userId}`)
            .then(response => response.json())
            .then(json => setPostListing(json));
    }, []);

    useEffect(() => {
        fetch(`${baseURL}/users`)
            .then(response => response.json())
            .then(json => setusersList(json));
    }, [])

    useEffect(() => {
        if (list.length > 0) return setRefreshing(false)

    }, [list]);

    const deletePost = (id) => {
        const _list = list && list.length > 0 && list.filter(x => x.id !== id);
        setPostListing(_list)
    }

    const onRefresh = React.useCallback(() => {
        fetch(`${baseURL}/posts?userId=${userId}`)
            .then(response => response.json())
            .then(json => setPostListing(json));
        setRefreshing(true);
    }, []);

    const addNewPost = () => {
        if (!post) return;
        const _newList = [...list, {
            body: post,
            title: `New Post ${list.length + 1}`,
            id: Math.random()
        }];
        setPostListing(_newList);
        if (_newList.length > 0) return setNewPost(null)
    }

    return (
        <>
            <ScrollView style={[styles.main, {
                paddingBottom: 40
            }]}
                refreshControl={
                    <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
                }
            >
                <View style={{
                    flex: 1,
                    marginTop: 10,
                    paddingBottom: 40
                }}>
                    {
                        !refreshing &&
                        <FlatList
                            data={list}
                            renderItem={({ item, index }) =>
                                <ListComponent
                                    userName={userName}
                                    item={item}
                                    onPress={() => {
                                        navigation.navigate('PostDetails', {
                                            body: item.body,
                                            postId: item.id,
                                            userName: userName,
                                            title: item.title
                                        })
                                    }
                                    }
                                    index={index}
                                    key={index}
                                    deletePost={() => deletePost(item.id)}
                                />}
                            showsHorizontalScrollIndicator={false}
                            keyExtractor={(item, i) => i.toString()}
                        />}
                </View>
            </ScrollView>
            <View style={styles.searchSection}>
                <TouchableOpacity
                    onPress={addNewPost}
                    style={styles.addPost}
                    activeOpacity={0.8}
                    hitSlop={{
                        top: 50,
                        bottom: 50,
                        left: 50,
                        right: 50
                    }}>
                    <Text style={{ color: '#fff' }}>Add</Text>
                </TouchableOpacity>
                <TextInput
                    style={styles.input}
                    placeholder="Add Post"
                    onChangeText={(post) => { setNewPost(post) }}
                    underlineColorAndroid="transparent"
                />
            </View>
        </>
    )

}

export default PostListing;

const styles = StyleSheet.create({
    main: {
        flex: 1,
        backgroundColor: '#fff',
        paddingBottom: 10

    },
    title: {
        fontSize: 16,
        fontWeight: '700',
        //    paddingVertical: 5
    },
    subTitle: {
        fontSize: 14,
        fontWeight: '600',
        //    paddingVertical: 5,
        flexWrap: 'wrap',
        width: '98%'
    },
    list: {
        marginHorizontal: 10,
        paddingVertical: 10,
        flexDirection: 'column',
        justifyContent: 'flex-start',
        borderWidth: 0.8,
        borderColor: 'rgba(0,0,0,0.5)',
        flex: 1,
        flexWrap: 'wrap',
        marginVertical: 10,
        paddingHorizontal: 12,
        borderRadius: 8

    },
    searchSection: {

        // marginHorizontal: 10,
        paddingHorizontal: 8,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
    },
    searchIcon: {

        width: 20,
        height: 20
    },
    input: {
        flex: 1,
        paddingTop: 10,
        paddingRight: 10,
        paddingBottom: 10,
        paddingLeft: 0,
        backgroundColor: '#fff',
        color: '#424242',
    },
    addPost: {
        backgroundColor: '#CA2234',
        padding: 10,
        position: 'absolute',
        zIndex: 100,
        right: 10,
        top: 10,
    }

})

