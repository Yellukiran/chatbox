import React, {useState, useEffect} from 'react';
import { View, Text, StyleSheet, ScrollView, TouchableWithoutFeedback } from 'react-native';
import { baseURL } from './../../config';

const HomeScreen = ({navigation, route}) => {
    const [refreshing, setRefreshing] = useState(false); 
    const [users, setusersList] = useState([]);
    
   useEffect(() => {
    fetch(`${baseURL}/users`)
    .then(response => response.json())
    .then(json => setusersList(json));
}, [])
    return(
         <View style={{flex:1,backgroundColor:'#fff'}}> 
             <ScrollView>
                 {
                     users && users.length > 0 && users.map((item,index)=>{
                         return(
                            <TouchableWithoutFeedback
                            onPress={() => {
                                navigation.navigate('PostListing', {
                                    userId: item.id,
                                    userName: item.name
                                })
                            }}>
                                <View
                                  style={{marginVertical:10,borderWidth:1,padding:10,marginHorizontal:10,borderRadius:10}}>
                                    <Text style={{fontWeight:'700'}}>{item.name}</Text>
                                    <Text>{item.phone}</Text>
                                    <Text>{item.website}</Text>
                                </View>
                                </TouchableWithoutFeedback>
                         )
                     })
                 }
             </ScrollView>
         </View>
    )
}

const styles = StyleSheet.create({
    main: {
        flex: 1,
        alignItems: 'center',
        // justifyContent: 'center',
        backgroundColor: '#fff'

    },
    title: {
       fontSize: 16,
       fontWeight: '700',
       paddingVertical: 5
    },
    subTitle: {
       fontSize: 14,
       fontWeight: '600',
       paddingVertical: 5
    },
    btn: {
        height: 50,
        width: '50%',
        borderRadius: 8,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        marginVertical: 4,
        flexDirection: 'row',
        backgroundColor: '#CA2234'
    
    }
})

export default HomeScreen;

