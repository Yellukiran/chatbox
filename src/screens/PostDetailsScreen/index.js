import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, TextInput, TouchableOpacity, ScrollView, FlatList, RefreshControl } from 'react-native';
import { baseURL } from './../../config';

const CommentSection = ({ userName, item, }) => {
    const { body } = item;
    return (

        <View style={styles.list}>
            <Text style={styles.title}
                ellipsizeMode={'tail'}
                numberOfLines={1}
            >{userName}</Text>
            <Text style={{ color: '#000', fontSize: 13, paddingVertical: 3 }}
                ellipsizeMode={'tail'}
            numberOfLines={1}>{body}</Text>
        </View>
    )
}

const PostDetails = ({ navigation, route }) => {
    const { body, postId, title, userName } = route.params || {};
    const [comments, setComments] = useState([]);
    const [isOpen, setOpen] = useState(false)

    useEffect(() => {
        fetch(`${baseURL}/comments?postId=${postId}`)
            .then(response => response.json())
            .then(json => setComments(json));
    }, []);
    // console.log(comments, "posts") 
    return (
        <View style={styles.main}>
            <View style={{ alignSelf: 'center', alignItems: 'center', marginVertical: 10, marginHorizontal: 10 }}>
                <Text
                    ellipsizeMode={'tail'}
                    numberOfLines={1}
                    style={{ fontSize: 20, fontWeight: 'bold' }}>{title}</Text>
            </View>
            <View style={styles.details}>
                <Text style={styles.subTitle}>{body}</Text>
                <TouchableOpacity
                    onPress={() => {
                        setOpen(!isOpen)
                    }}
                    activeOpacity={0.9}>
                    <View style={styles.comments}>
                        <Text style={{
                            color: '#000',
                            fontSize: 18,
                            fontWeight: '700'
                        }}>{comments.length} Comments</Text>
                    </View>
                </TouchableOpacity>
                {
                    isOpen &&
                    <View style={{ flex: 1, marginTop: 10, marginBottom: 10 }}>
                        <FlatList
                            data={comments}
                            renderItem={({ item, index }) =>
                                <CommentSection
                                    userName={userName}
                                    key={index}
                                    item={item}
                                    title={title}
                                />
                            }
                            showsHorizontalScrollIndicator={false}
                            keyExtractor={(item, i) => i.toString()}
                        />
                    </View>
                }
            </View>

        </View>
    )

}

export default PostDetails;

const styles = StyleSheet.create({
    main: {
        flex: 1,
        backgroundColor: '#fff'

    },
    details: {
        flex: 1,
        flexDirection: 'column',
        marginTop: 10,
        marginBottom: 10,
        paddingHorizontal: 20,

    },
    title: {
        fontSize: 16,
        fontWeight: '700',
    },
    subTitle: {
        fontSize: 16,
        fontWeight: '600',
        flex: 1,
        textAlign:'justify'
    },
    list: {
        flex: 1,
        marginHorizontal: 10,
        flexDirection: 'column',
        paddingHorizontal: 5

    },
    comments: {
        height: 50,
        width: '50%',
        borderRadius: 8,
        // justifyContent: 'center',
        // alignItems: 'center',
        // alignSelf: 'center',
        // marginVertical: 4,
        flexDirection: 'row',
        // backgroundColor: '#CA2234'
    },
    commentSection: {
        marginVertical: 10,
        paddingHorizontal: 12,
    }

})